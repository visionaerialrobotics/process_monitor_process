/*!*******************************************************************************************
 *  \file       process_monitor_process_main.cpp
 *  \brief      ProcessMonitor main file.
 *  \details    This file includes the ProcessMonitor main declaration.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/
#include "process_monitor_process.h"

//! This main do not follow the usual pattern to execute a DroneProcess class, due to its class nature.
int main(int argc, char **argv)
{
  ProcessMonitor pMonitor (argc, argv);    
}
