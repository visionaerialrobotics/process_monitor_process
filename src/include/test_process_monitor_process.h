/*!*******************************************************************************************
 *  \file       test_process_monitor_process.h
 *  \brief      ProcessMonitorTest definition file.
 *  \details    This file contains the ProcessMonitorTest declaration. To obtain more information about
 *              it's definition consult the yest_process_monitor_process.cpp file.
 *  \author     Enrique Ortiz
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <aerostack_msgs/AliveSignal.h>
#include <aerostack_msgs/ProcessError.h>
#include <droneMsgsROS/askForModule.h>
#include <std_srvs/Trigger.h>
#include <string>
#include <stdio.h>

 /*!******************************************************************************************
 *  \class      ProcessMonitorTest
 *  \brief      This class has the required implementeation to check the process monitor operation.
 *  \details    The ProcessMonitorTest has three tests. The first and second one checks if 
 *              the process monitor is able to detect a node death an its later restoration.
 *              The third test checks if the process monitor is able to recieve and resend
 *              the errors detected by any node.\n
 *              All the test results are showed through std output.
 *********************************************************************************************/

#define COLOR_RED     "\x1b[31m" //!< Color code value in the terminal
#define COLOR_GREEN   "\x1b[32m" //!< Color code value in the terminal
#define COLOR_ORANGE  "\x1b[33m" //!< Color code value in the terminal
#define COLOR_RESET   "\x1b[0m"  //!< Color code value in the terminal
#define NUM_TEST 6               //!< Number of implemented tests at the moment 

class ProcessMonitorTest
{
public:
  //! Constructor. \details The ProcessMonitorTest constructor doesn't need any argument.
  ProcessMonitorTest(int argc, char **argv);
  ~ProcessMonitorTest();

  /*!*************************************************************************************
   * \details Validate is the function to be called after the object instantiation.
   * \return All the validation results are displayed throough stdout.
   ****************************************************************************************/
  void validate();

private:
  ros::Subscriber error_sub;                    //!< ROS publisher handler used to receive error messages
  ros::Publisher watchdog_pub;                  //!< ROS publisher handler used to send state messages.
  aerostack_msgs::AliveSignal watchdog_message;   //!< Message used in watchdog_pub topic 

  ros::Publisher error_pub;                     //!< ROS publisher handler used to send error messages.
  aerostack_msgs::ProcessError error_message;     //!< Message used in error_pub/error_sub topic

  ros::ServiceClient wifiIsOkClient;
  ros::ServiceClient moduleIsOnlineClient;
  ros::ServiceClient moduleIsStartedClient;

  bool all_ok;                           //!< Attribute storing if all tests run correctly or someone detects an errors
  bool death_detected;                   //!< Temporal attribute storing if a DroneProcess is alive or not
  bool node_restoration;                 //!< Temporal attribute storing if a DroneProcess is restoring or not
  bool node_error_detection;             //!< Temporal attribute storing if a DroneProcess has an error or not
  std::string hostname;                  //!< Attribute storing the computer hostname where runs the ProcessMonitor


  /*!***************************************************************************************
   * \details The first test checks the process monitor ability to detect a node death
   *          by sending the first alive message and waiting till the error is sent by the
   *          process monitor.
   ****************************************************************************************/
  bool test1();

    /*!*************************************************************************************
   * \details The second test checks the process monitor ability to reincorporate a 
   *          previously death node by sending a second alive message after a death error
   *          message is recieved.
   ****************************************************************************************/
  void test2();

  /*!*************************************************************************************
   * \details The third test checks if the supervisor resends an error message
   *          previously send by a node. 
   ****************************************************************************************/
  void test3();

  /*!*************************************************************************************
   * \details The fourth test checks the moduleIsOnline service.
   ****************************************************************************************/
  void test4();

  /*!*************************************************************************************
   * \details The fifth test checks the moduleIsStarted service.
   ****************************************************************************************/
  void test5();

  /*!*************************************************************************************
   * \details The sixth test checks the wifiIsOk service.
   ****************************************************************************************/
  void test6();

  /*!*************************************************************************************
   * \details The callback function is responsible of the error message obtainment.
   ****************************************************************************************/
  void errorInformerCallback(const aerostack_msgs::ProcessError::ConstPtr& msg);
};